**subtree-updater** is a tool that pulls changes from a source-repository using [git subtree](https://github.com/git/git/blob/master/contrib/subtree/git-subtree.txt), updates dependent repositories and creates pull requests for all of them.

*subtree-updater* is packaged as [docker container](https://gitlab.com/ittennull/subtree-updater/container_registry)

For documentation refer to [wiki pages](https://gitlab.com/ittennull/subtree-updater/wikis/home)
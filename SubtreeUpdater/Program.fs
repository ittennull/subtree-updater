open System
open Microsoft.Extensions.Logging
open Serilog
open Serilog.Extensions.Logging
open SubtreeUpdater.Core.Types
open SubtreeUpdater.Core.ActivePatterns

type Options = {
    [<CommandLine.Option("git-system", Required = false, HelpText = "Type of version control system when it's not possible to infer it from argument '--subtree-url'. Possible values: azuredevops, github, gitlab")>] gitSystem : string option;
    [<CommandLine.Option("pat", Required = true, HelpText = "Personal Access Token")>] pat : string;
    [<CommandLine.Option("subtree-url", Required = true, HelpText = "Url of subtree repository")>] subtreeUrl : Uri;
    [<CommandLine.Option("exact-location", Required = false, SetName = "exact-location", HelpText = "Relative path to subtree directory")>] exactLocation : string option;
    [<CommandLine.Option("path-ends-with", Required = false, SetName = "path-ends-with", HelpText = "Find subtree directory by searching for a path that ends this value")>] pathEndsWith : string option;
    [<CommandLine.Option("repository-filters", Required = false, Separator = ',', HelpText = "Take only repositories that match this filter. Wildcards * and ? are supported. Multiple filters can be specified separated by comma")>] repositoryNameFilters : string seq;
    [<CommandLine.Option("complete-pr", Required = false, Default = false, HelpText = "Complete pull request after creating it. Can fail if for example there is a branch policy that requires minimum number of approvals. In that case pull request will still be created")>] completePullRequest : bool;
    [<CommandLine.Option("dry-run", Required = false, Default = false, HelpText = "Don't push new branches and don't create pull requests")>] dryRun : bool;
    [<CommandLine.Option("azure-devops-project-filters", Required = false, Separator = ',', HelpText = "Take only Azure DevOps projects that match these filters. Wildcards * and ? are supported. Multiple filters can be specified separated by comma")>] devOpsProjectNameFilters : string seq;
    [<CommandLine.Option("github-org-filters", Required = false, Separator = ',', HelpText = "Take only GitHub organizations that match these filters. Wildcards * and ? are supported. Multiple filters can be specified separated by comma")>] gitHubOrganizationNameFilters : string seq;
    [<CommandLine.Option("gitlab-group-filters", Required = false, Separator = ',', HelpText = "Take only GitLab project groups that match these filters. Wildcards * and ? are supported. Multiple filters can be specified separated by comma")>] gitLabGroupNameFilters : string seq;
}

let printResults (logger: Microsoft.Extensions.Logging.ILogger) updateResults =
    logger.LogInformation "==================================="
        
    let printSection title patterns print =
        let selector x =
            patterns
            |> Seq.map ((|>) x)
            |> Seq.choose id
            |> Seq.tryHead
            
        let list = updateResults |> List.choose selector
        if not (List.isEmpty list) then
            logger.LogInformation title
            list |> List.iter print
            
    printSection "Completed PRs:" [(|PullRequestCompleted|_|)]
        (fun (result, url) -> logger.LogInformation("🗹\t{0} - {1}", result.Repository, url))
    
    printSection "Created or updated PRs:" [(|PullRequestCreated|_|); (|PullRequestUpdated|_|); (|PullRequestUnchanged|_|)]
        (fun (result, url) -> logger.LogInformation("✓\t{0} - {1}", result.Repository, url))
    
    printSection "Merge conflicts:" [(|MergeConflict|_|)]
        (fun result -> logger.LogInformation("✕\t{0} - subtree path: '{1}'", result.Repository, result.Subtree))
    
    printSection "Nothing to do. The subtrees are up to date:" [(|NothingToDo|_|)]
        (fun result -> logger.LogInformation("=\t{0} - subtree path: '{1}'", result.Repository, result.Subtree))
        
    printSection "Errors:" [(|UpdateError|_|)]
        (fun (result, error) -> logger.LogInformation("☒\t{0} - subtree path: '{1}', error: {2}", result.Repository, result.Subtree, error))

let run options =
    let searchFor =
        match options.exactLocation, options.pathEndsWith with
        | Some path, None -> ExactLocation path
        | None, Some pathEndsWith -> PathEndsWith pathEndsWith
        | _ -> failwith "Either one 'exact-location' or 'path-ends-with' must be specified"
        
    let repositoryGroupFilters =
        let notEmptyFilters =
            [ options.devOpsProjectNameFilters
              options.gitHubOrganizationNameFilters
              options.gitLabGroupNameFilters ]
            |> List.filter (Seq.isEmpty >> not)
        
        match notEmptyFilters.Length with
        | n when n <= 1 -> notEmptyFilters |> List.tryHead |> Option.defaultValue Seq.empty 
        | _ -> failwith "Either single one of 'azure-devops-project-filters' or 'github-org-filters' or 'gitlab-group-filters' may be specified"

    let gitSystem =
        options.gitSystem |> Option.map(function
            | "azuredevops" -> AzureDevOps
            | "github" -> GitHub
            | "gitlab" -> GitLab
            | _ -> failwith "Unsupported value of '--git-system' parameter")
        
    use serilog =
        LoggerConfiguration()
            .WriteTo.Console(Serilog.Events.LogEventLevel.Verbose, "{Message:lj}{NewLine}{Exception}")
            .CreateLogger();
    let logger =
        use loggerProvider = new SerilogLoggerProvider(serilog, false)
        loggerProvider.CreateLogger("")
        
    let runParameters = {
        GitSystem = gitSystem
        Pat = options.pat
        SubtreeUrl = options.subtreeUrl
        SearchFor = searchFor
        DryRun = options.dryRun
        RepositoryGroupFilters = repositoryGroupFilters
        RepositoryNameFilters = options.repositoryNameFilters
        CompletePullRequests = options.completePullRequest
    }

    let updateResults = SubtreeUpdater.Core.Runner.run runParameters logger
    printResults logger updateResults
      
[<EntryPoint>]
let main argv =
    let result = CommandLine.Parser.Default.ParseArguments<Options>(argv)
    match result with
    | :? CommandLine.Parsed<Options> as parsed -> run parsed.Value
    | _ -> ()
    
    0

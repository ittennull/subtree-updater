module internal SubtreeUpdater.Core.AzureDevOps.Interface

open System
open SubtreeUpdater.Core.Utils
open Microsoft.VisualStudio.Services.Common
open Microsoft.VisualStudio.Services.WebApi
open Microsoft.TeamFoundation.Core.WebApi
open Microsoft.TeamFoundation.SourceControl.WebApi

let getAzureDevOpsFunctions logger pat (subtreeUri: Uri) =
    let orgBaseUrl =
        if subtreeUri.Segments.Length < 2 then
            failwithf "Subtree url is too short, can't extract organization base url from '%s'" subtreeUri.AbsoluteUri
        Uri(subtreeUri.Scheme + "://" + subtreeUri.Host + subtreeUri.Segments.[0] + subtreeUri.Segments.[1])
    
    let credentials = VssCredentials(VssBasicCredential("", pat))
    let connection = new VssConnection(orgBaseUrl, credentials)
    let gitClient = connection.GetClient<GitHttpClient>()
    let subtreeRepoName = getSubtreeRepositoryName subtreeUri.AbsolutePath
    
    let getRepositories groupNameFilters repositoryNameFilters =
        use projectClient = connection.GetClient<ProjectHttpClient>();
        RepositoryList.getRepositories projectClient gitClient groupNameFilters repositoryNameFilters
    let dispose () =
        gitClient.Dispose()
        connection.Dispose()
        
    {|  GetMapOfSubtreeDirToPullRequest = PullRequest.getMapOfSubtreeDirToPullRequest gitClient subtreeRepoName
        CreatePullRequest = PullRequest.createPullRequest gitClient subtreeRepoName
        CompletePullRequest = PullRequest.completePullRequest logger gitClient
        GetRepositories = getRepositories
        Dispose = dispose |}
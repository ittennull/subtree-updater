module internal SubtreeUpdater.Core.AzureDevOps.PullRequest

open System
open System.Threading
open LibGit2Sharp
open Microsoft.Extensions.Logging
open Microsoft.TeamFoundation.Core.WebApi
open Microsoft.TeamFoundation.SourceControl.WebApi
open Microsoft.VisualStudio.Services.WebApi.Patch
open Microsoft.VisualStudio.Services.WebApi.Patch.Json
open SubtreeUpdater.Core.Types.Internal
open SubtreeUpdater.Core.PullRequestAdditionalData

let private additionalDataProperty = "subtree-updater-data"

let private getPullRequestUrl (gitRepository: GitRepository) (pullRequest: GitPullRequest) =
    gitRepository.WebUrl + "/pullrequest/" + pullRequest.PullRequestId.ToString()
    
let private createDomainPullRequest gitRepository gitPullRequest =
    {
        Url = getPullRequestUrl gitRepository gitPullRequest
        BranchName = gitPullRequest.SourceRefName
        Custom = CustomPullRequest.AzureDevOps gitPullRequest }

let createPullRequest (gitClient: GitHttpClient) subtreeRepoName (gitRepository: GitRepository) subtreeDirectory (branch: Branch) =
    let addPullRequestIdentificationProperty pullRequestId =
        let additionalData = createAdditionalDataString subtreeRepoName subtreeDirectory
        let patch = JsonPatchDocument()
        patch.Add(JsonPatchOperation(Operation = Operation.Add, Path = "/" + additionalDataProperty, Value = additionalData))
        gitClient.UpdatePullRequestPropertiesAsync(patch, gitRepository.Id, pullRequestId).Result
    
    let gitPullRequest = 
        GitPullRequest(
            SourceRefName = branch.CanonicalName,
            TargetRefName = gitRepository.DefaultBranch,
            Title = "Apply changes from " + subtreeRepoName,
            Labels = [| WebApiTagDefinition(Name = subtreeRepoName) |] )
    
    try
        let pr = gitClient.CreatePullRequestAsync(gitPullRequest, gitRepository.Id, Nullable<bool>()).Result
        addPullRequestIdentificationProperty pr.PullRequestId |> ignore
        
        Ok <| createDomainPullRequest gitRepository pr
    with
        | ex -> Error ex.Message 

let private waitForMergeStatus (logger: ILogger) (gitClient: GitHttpClient) (gitPullRequest: GitPullRequest) =
    let rec checkMergeStatus n =
        let pr = gitClient.GetPullRequestAsync(gitPullRequest.Repository.Id, gitPullRequest.PullRequestId).Result
        match pr.MergeStatus with
        | PullRequestAsyncStatus.Succeeded -> Ok ()
        | PullRequestAsyncStatus.Conflicts
        | PullRequestAsyncStatus.Failure
        | PullRequestAsyncStatus.RejectedByPolicy ->
            logger.LogWarning("Pull request cannot be merged: {MergeStatus}", pr.MergeStatus)
            Error ()
        | _ ->
            if n = 0 then
                logger.LogWarning("Pull request still doesn't have required PullRequestAsyncStatus.Succeeded status")
                Error ()
            else
                logger.LogInformation("Pull request {0} has merge status '{1}', expected status '{2}'. Waiting...", gitPullRequest.PullRequestId, pr.MergeStatus, PullRequestAsyncStatus.Succeeded)
                System.Threading.Thread.Sleep(1000)
                checkMergeStatus (n - 1)
    
    checkMergeStatus 10

let completePullRequest (logger: ILogger) (gitClient: GitHttpClient) (pullRequest: GitPullRequest) =
    let updatedPr =
      GitPullRequest(
          Status = PullRequestStatus.Completed,
          LastMergeSourceCommit = pullRequest.LastMergeSourceCommit )
    
    try
        waitForMergeStatus logger gitClient pullRequest
        |> Result.bind (fun _ -> 
            gitClient.UpdatePullRequestAsync(updatedPr, pullRequest.Repository.Id, pullRequest.PullRequestId).Result |> ignore
            Ok () )
    with
        | ex ->
            let msg = ex.GetBaseException().Message |> sprintf "%s"
            logger.LogWarning("Couldn't complete PR: {0}", msg)
            Error ()

let getMapOfSubtreeDirToPullRequest (gitClient: GitHttpClient) subtreeRepoName (gitRepository: GitRepository) =
    let getAllPullRequests () = gitClient.GetPullRequestsAsync(gitRepository.Id, GitPullRequestSearchCriteria()).Result
    let getAdditionalData (pr: GitPullRequest) =
        let properties = gitClient.GetPullRequestPropertiesAsync(gitRepository.Id, pr.PullRequestId, null, CancellationToken.None).Result
        let exists, value = properties.TryGetValue additionalDataProperty
        if exists then (value :?> string) else String.Empty
    let createDomainPullRequest = createDomainPullRequest gitRepository
    getMapOfSubtreeDirToPullRequest subtreeRepoName getAllPullRequests getAdditionalData createDomainPullRequest

module internal SubtreeUpdater.Core.AzureDevOps.RepositoryList

open System
open Microsoft.TeamFoundation.SourceControl.WebApi
open Microsoft.TeamFoundation.Core.WebApi
open SubtreeUpdater.Core.Types.Internal

let private toRemoteRepository (gitRepository: GitRepository) =
    {
        Name = gitRepository.Name
        Uri = Uri(gitRepository.RemoteUrl)
        DefaultBranchName = gitRepository.DefaultBranch
        GroupName = gitRepository.ProjectReference.Name
        Custom = AzureDevOps gitRepository
    }
    
let getRepositories (projectClient: ProjectHttpClient) (gitClient: GitHttpClient) projectFilter repositoryFilter =
    let projects = projectClient.GetProjects().Result;
    projects
    |> Seq.filter (fun x-> projectFilter x.Name)
    |> Seq.map (fun project -> gitClient.GetRepositoriesAsync(project.Id).Result)
    |> Seq.collect id
    |> Seq.filter (fun x -> repositoryFilter x.Name)
    |> Seq.map toRemoteRepository
    |> Seq.toList

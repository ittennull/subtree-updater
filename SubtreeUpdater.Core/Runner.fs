module SubtreeUpdater.Core.Runner

open System.IO
open Microsoft.Extensions.Logging
open Microsoft.TeamFoundation.SourceControl.WebApi
open SubtreeUpdater.Core.SubtreeDirectoryFinder
open SubtreeUpdater.Core.Repository
open SubtreeUpdater.Core.RepositoryUpdater
open SubtreeUpdater.Core.Types
open SubtreeUpdater.Core.Types.Internal
open SubtreeUpdater.Core.Utils

let private getGitSystem (gitSystem: GitSystem option) (url: string) =
    gitSystem |> Option.defaultWith (fun () ->
        if url.Contains("azure.com") then GitSystem.AzureDevOps
        elif url.Contains("github.com") then GitSystem.GitHub
        elif url.Contains("gitlab.com") then GitSystem.GitLab
        else failwithf "Can't infer Git system from url '%s'" url)
    
let run parameters (logger : ILogger) =
    let azureDevOpsFunctions = lazy AzureDevOps.Interface.getAzureDevOpsFunctions logger parameters.Pat parameters.SubtreeUrl
    let gitHubFunctions = lazy GitHub.Interface.getGitHubFunctions logger parameters.Pat parameters.SubtreeUrl
    let gitLabFunctions = lazy GitLab.Interface.getGitLabFunctions logger parameters.Pat parameters.SubtreeUrl
    
    let createPullRequest =
        match parameters.DryRun with
        | true -> fun _ _ _ -> Ok <| {
            Url = "https://dry-run.com/pullrequest"
            BranchName = "not-a-real-branch"
            Custom = GitPullRequest() |> CustomPullRequest.AzureDevOps }
        | false ->
            fun remoteRepository ->
                match remoteRepository.Custom with
                | CustomRemoteRepository.AzureDevOps repo -> azureDevOpsFunctions.Value.CreatePullRequest repo
                | CustomRemoteRepository.GitHub repo -> gitHubFunctions.Value.CreatePullRequest repo
                | CustomRemoteRepository.GitLab project -> gitLabFunctions.Value.CreatePullRequest project
           
    let pushBranch =
        match parameters.DryRun with
        | true -> fun _ _ -> ()
        | false -> pushBranch parameters.Pat
    
    let completePullRequest =
        match parameters.DryRun || not parameters.CompletePullRequests with
        | true -> None
        | false ->
            let complete (pullRequest: PullRequest) =
                match pullRequest.Custom with
                | CustomPullRequest.AzureDevOps pr -> azureDevOpsFunctions.Value.CompletePullRequest pr
                | CustomPullRequest.GitHub pr -> gitHubFunctions.Value.CompletePullRequest pr
                | CustomPullRequest.GitLab mr -> gitLabFunctions.Value.CompletePullRequest mr
            Some complete
            
    let getMapOfSubtreeDirToPullRequest remoteRepository =
        match remoteRepository.Custom with
        | CustomRemoteRepository.AzureDevOps repo -> azureDevOpsFunctions.Value.GetMapOfSubtreeDirToPullRequest repo
        | CustomRemoteRepository.GitHub repo -> gitHubFunctions.Value.GetMapOfSubtreeDirToPullRequest repo
        | CustomRemoteRepository.GitLab project -> gitLabFunctions.Value.GetMapOfSubtreeDirToPullRequest project

    logger.LogInformation("Getting list of repositories")
    let repositories =
        let getRepositories = 
            match getGitSystem parameters.GitSystem parameters.SubtreeUrl.AbsoluteUri with
            | GitSystem.AzureDevOps -> azureDevOpsFunctions.Value.GetRepositories
            | GitSystem.GitHub -> gitHubFunctions.Value.GetRepositories
            | GitSystem.GitLab -> gitLabFunctions.Value.GetRepositories
            
        let projectFilter = createFilter parameters.RepositoryGroupFilters
        let repositoryFilter = createFilter parameters.RepositoryNameFilters

        getRepositories projectFilter repositoryFilter
    
    let cloneRepository = cloneRepository parameters.Pat
    
    logger.LogInformation("Cloning subtree repository")
    use subtreeRepository = cloneRepository parameters.SubtreeUrl Bare
    
    let findSubtreeDirectories = findSubtreeDirectories parameters.SearchFor
    let runProcess = ProcessRunner.runProcess logger
    let subtreePull = subtreePull runProcess subtreeRepository.Info.Path
    
    let updateResults = 
        repositories
        |> List.map (fun gitRepository ->
            logger.LogInformation("Processing '{0}: {1}'", gitRepository.GroupName, gitRepository.Name)
            updateRepository
                gitRepository
                cloneRepository
                findSubtreeDirectories
                getMapOfSubtreeDirToPullRequest
                subtreePull
                pushBranch
                createPullRequest
                completePullRequest
            )
        |> List.collect id
        
    Directory.Delete(subtreeRepository.Info.Path, true)
    
    if azureDevOpsFunctions.IsValueCreated then azureDevOpsFunctions.Value.Dispose()
    
    updateResults
module internal SubtreeUpdater.Core.Repository

open System
open System.IO
open LibGit2Sharp

type CloneOption = Bare | Normal

let private getShortBranchName (branchName: string) =
    let start = "refs/heads/"
    match branchName.StartsWith start with
    | true -> branchName.[start.Length ..]
    | false -> branchName
    
let private getCredentials pat: LibGit2Sharp.Credentials =
    UsernamePasswordCredentials(Username = "subtree-updater", Password = pat) :> LibGit2Sharp.Credentials

let cloneRepository pat (repositoryUri: Uri) cloneOption =
    let directory = Path.Combine(Path.GetTempPath(), "subtree_updater", Guid.NewGuid().ToString())
    Directory.CreateDirectory(directory) |> ignore
    
    let isBare = match cloneOption with Bare -> true | Normal -> false
    
    let cloneOptions =
        CloneOptions(
            IsBare = isBare,
            CredentialsProvider = fun _ _ _ -> getCredentials pat
        )
    
    Repository.Clone(repositoryUri.AbsoluteUri, directory, cloneOptions) |> ignore
    new Repository(directory)

let private checkout (repo: Repository) (branchName: string) (fromCommittish: string)=
    let updateBranch upstreamBranch (bu: BranchUpdater) =
        bu.Remote <- "origin"
        bu.UpstreamBranch <- upstreamBranch
        
    let branch = repo.CreateBranch(branchName, fromCommittish)
    let branch = Commands.Checkout(repo, branch)
    repo.Branches.Update(branch, updateBranch branch.CanonicalName)

let createBranch (repo: Repository) (branchName: string) =
    let newBranchName = "update-subtree-" + Guid.NewGuid().ToString()
    let branchName' = getShortBranchName  branchName
    checkout repo newBranchName branchName'

let checkOutBranch (repo: Repository) (branchName: string) =
    let branchName' = getShortBranchName  branchName
    let remoteBranchName = "origin/" + branchName'
    checkout repo branchName' remoteBranchName

let setUserNameAndEmailForRepository (repo: Repository) =
    repo.Config.Set("user.name", "subtree-updater")
    repo.Config.Set("user.email", "subtree-updater@automation.dev", ConfigurationLevel.Local)

let subtreePull runProcess source destination workingDirectory =
    let args = sprintf "subtree pull -P %s --squash %s master" destination source
    runProcess workingDirectory "git" args

let pushBranch pat (repo: Repository) (branch: Branch) =
    let pushOptions = PushOptions(CredentialsProvider = fun _ _ _ -> getCredentials pat)
    repo.Network.Push(branch, pushOptions)

let lastCommitSha (branch: Branch) =
    branch.Commits |> Seq.head |> (fun x -> x.Tree.Sha)
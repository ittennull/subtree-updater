module internal SubtreeUpdater.Core.GitLab.Interface

open System
open GitLabApiClient
open SubtreeUpdater.Core.Utils

let getGitLabFunctions logger (pat: string) (subtreeUri: Uri) =
    let hostUrl = subtreeUri.Scheme + "://" + subtreeUri.Host
        
    let client = new GitLabClient(hostUrl, pat);
    let subtreeRepoName = getSubtreeRepositoryName subtreeUri.AbsolutePath
    
    {|  GetMapOfSubtreeDirToPullRequest = PullRequest.getMapOfSubtreeDirToPullRequest client.MergeRequests subtreeRepoName
        CreatePullRequest = PullRequest.createPullRequest client.MergeRequests subtreeRepoName
        CompletePullRequest = PullRequest.completePullRequest logger client.MergeRequests
        GetRepositories = RepositoryList.getRepositories client.Projects |}
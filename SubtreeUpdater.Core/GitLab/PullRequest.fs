module internal SubtreeUpdater.Core.GitLab.PullRequest

open System
open GitLabApiClient
open GitLabApiClient.Models.MergeRequests.Requests
open GitLabApiClient.Models.MergeRequests.Responses
open GitLabApiClient.Models.Projects.Responses
open Microsoft.Extensions.Logging
open SubtreeUpdater.Core.Types.Internal
open SubtreeUpdater.Core.PullRequestAdditionalData

let private createDomainPullRequest (mergeRequest: MergeRequest) =
    { Url = mergeRequest.WebUrl
      BranchName = mergeRequest.SourceBranch
      Custom = CustomPullRequest.GitLab mergeRequest }
    
let getProjectIdFromProject (project: Project) =
    Internal.Paths.ProjectId.op_Implicit project.Id
    
let getProjectIdFromMergeRequest (mergeRequest: MergeRequest) =
    Internal.Paths.ProjectId.op_Implicit mergeRequest.ProjectId

let createPullRequest (mergeRequestsClient: MergeRequestsClient) subtreeRepoName (project: Project) subtreeDirectory (branch: LibGit2Sharp.Branch) =
    let title = "Apply changes from " + subtreeRepoName
    let projectId = getProjectIdFromProject project
    
    let createMergeRequest = CreateMergeRequest(branch.FriendlyName, project.DefaultBranch, title)
    createMergeRequest.Description <- createAdditionalDataString subtreeRepoName subtreeDirectory
    createMergeRequest.RemoveSourceBranch <- Nullable(true)
    
    try
        let mergeRequest = mergeRequestsClient.CreateAsync(projectId, createMergeRequest).Result
        Ok <| createDomainPullRequest mergeRequest
    with
        | ex -> Error ex.Message 

let completePullRequest (logger: ILogger) (mergeRequestsClient: MergeRequestsClient) (mergeRequest: MergeRequest) =
    try
        let projectId = getProjectIdFromMergeRequest mergeRequest
        let acceptMergeRequest = AcceptMergeRequest(MergeWhenPipelineSucceeds = Nullable(true))
        mergeRequestsClient.AcceptAsync(projectId, mergeRequest.Iid, acceptMergeRequest).Result |> ignore
        Ok ()
    with
        | ex ->
            let msg = ex.GetBaseException().Message |> sprintf "%s"
            logger.LogWarning("Couldn't complete PR: {0}", msg)
            Error ()

let getMapOfSubtreeDirToPullRequest (mergeRequestsClient: MergeRequestsClient) subtreeRepoName (project: Project) =
    let projectId = getProjectIdFromProject project
    let getAllMergeRequests () = mergeRequestsClient.GetAsync(projectId).Result
    let getAdditionalData (mergeRequest: MergeRequest) = mergeRequest.Description
    getMapOfSubtreeDirToPullRequest subtreeRepoName getAllMergeRequests getAdditionalData createDomainPullRequest

module internal SubtreeUpdater.Core.GitLab.RepositoryList

open System
open GitLabApiClient
open SubtreeUpdater.Core.Types.Internal

let private getGroupName (pathWithNamespace: string) =
    pathWithNamespace.[.. pathWithNamespace.LastIndexOf('/') - 1]

let private toRemoteRepository (gitLabProject: Models.Projects.Responses.Project) =
    {
        Name = gitLabProject.Name
        Uri = Uri(gitLabProject.HttpUrlToRepo)
        DefaultBranchName = gitLabProject.DefaultBranch
        GroupName = getGroupName gitLabProject.PathWithNamespace
        Custom = GitLab gitLabProject
    }
    
let getRepositories (projectsClient: ProjectsClient) projectFilter repositoryFilter =
    let projectQueryOptions (queryOptions: Models.Projects.Requests.ProjectQueryOptions) =
        queryOptions.Owned <- true
        queryOptions.IsMemberOf <- true
    
    let projects = projectsClient.GetAsync(projectQueryOptions).Result

    projects
    |> Seq.filter (fun x-> getGroupName x.PathWithNamespace |> projectFilter)
    |> Seq.filter (fun x -> repositoryFilter x.Name)
    |> Seq.map toRemoteRepository
    |> Seq.toList
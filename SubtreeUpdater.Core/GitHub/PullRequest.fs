module internal SubtreeUpdater.Core.GitHub.PullRequest

open Microsoft.Extensions.Logging
open Octokit
open SubtreeUpdater.Core.Types.Internal
open SubtreeUpdater.Core.PullRequestAdditionalData

let private createDomainPullRequest (pullRequest: Octokit.PullRequest) =
    { Url = pullRequest.HtmlUrl
      BranchName = pullRequest.Head.Ref
      Custom = CustomPullRequest.GitHub pullRequest }

let createPullRequest (pullRequestClient: IPullRequestsClient) subtreeRepoName (gitHubRepository: Octokit.Repository) subtreeDirectory (branch: LibGit2Sharp.Branch) =
    let title = "Apply changes from " + subtreeRepoName
    let newPullRequest = NewPullRequest(title, branch.CanonicalName, gitHubRepository.DefaultBranch)
    newPullRequest.Body <- createAdditionalDataString subtreeRepoName subtreeDirectory
    
    try
        let pr = pullRequestClient.Create(gitHubRepository.Id, newPullRequest).Result
        Ok <| createDomainPullRequest pr
    with
        | ex -> Error ex.Message 

let completePullRequest (logger: ILogger) (pullRequestClient: IPullRequestsClient) (pullRequest: Octokit.PullRequest) =
    try
        pullRequestClient.Merge(pullRequest.Base.Repository.Id, pullRequest.Number, MergePullRequest()).Result |> ignore
        Ok ()
    with
        | ex ->
            let msg = ex.GetBaseException().Message |> sprintf "%s"
            logger.LogWarning("Couldn't complete PR: {0}", msg)
            Error ()

let getMapOfSubtreeDirToPullRequest (pullRequestClient: IPullRequestsClient) subtreeRepoName (gitHubRepository: Octokit.Repository) =
    let getAllPullRequests () = pullRequestClient.GetAllForRepository(gitHubRepository.Id).Result
    let getAdditionalData (pr: Octokit.PullRequest) = pr.Body
    getMapOfSubtreeDirToPullRequest subtreeRepoName getAllPullRequests getAdditionalData createDomainPullRequest

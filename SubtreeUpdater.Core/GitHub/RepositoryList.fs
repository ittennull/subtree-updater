module internal SubtreeUpdater.Core.GitHub.RepositoryList

open System
open Octokit
open SubtreeUpdater.Core.Types.Internal

let private toRemoteRepository (gitHubRepository: Repository) =
    {
        Name = gitHubRepository.Name
        Uri = Uri(gitHubRepository.CloneUrl)
        DefaultBranchName = gitHubRepository.DefaultBranch
        GroupName = gitHubRepository.Owner.Login
        Custom = GitHub gitHubRepository
    }
    
let getRepositories (organizationsClient: IOrganizationsClient) (repositoriesClient: IRepositoriesClient) projectFilter repositoryFilter =
    let projects = organizationsClient.GetAllForCurrent().Result;
    projects
    |> Seq.filter (fun x-> projectFilter x.Login)
    |> Seq.map (fun org -> repositoriesClient.GetAllForOrg(org.Login).Result)
    |> Seq.collect id
    |> Seq.filter (fun x -> repositoryFilter x.Name)
    |> Seq.map toRemoteRepository
    |> Seq.toList
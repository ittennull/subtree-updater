module internal SubtreeUpdater.Core.GitHub.Interface

open System
open Octokit
open SubtreeUpdater.Core.Utils

let getGitHubFunctions logger (pat: string) (subtreeUri: Uri) =
    let client = new GitHubClient(new ProductHeaderValue("subtree-updater"));
    let basicAuth = Credentials("subtree-updater", pat);
    client.Credentials <- basicAuth;
    let subtreeRepoName = getSubtreeRepositoryName subtreeUri.AbsolutePath
    
    {|  GetMapOfSubtreeDirToPullRequest = PullRequest.getMapOfSubtreeDirToPullRequest client.PullRequest subtreeRepoName
        CreatePullRequest = PullRequest.createPullRequest client.PullRequest subtreeRepoName
        CompletePullRequest = PullRequest.completePullRequest logger client.PullRequest
        GetRepositories = RepositoryList.getRepositories client.Organization client.Repository |}
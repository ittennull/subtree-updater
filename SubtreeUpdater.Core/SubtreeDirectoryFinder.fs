module internal SubtreeUpdater.Core.SubtreeDirectoryFinder

open System.IO
open SubtreeUpdater.Core.Types

let findSubtreeDirectories searchFor rootDirectory =
    let entries = Directory.GetFileSystemEntries(rootDirectory, "*", SearchOption.AllDirectories)
    entries
    |> Seq.map (fun x -> x.[rootDirectory.Length ..])
    |> Seq.filter (fun x ->
        match searchFor with
        | ExactLocation path -> x = path
        | PathEndsWith pathEnd -> x.EndsWith(pathEnd)
        )
    |> Seq.map (fun path ->
        let fullPath = rootDirectory + path
        let attributes = File.GetAttributes(fullPath)
        if attributes.HasFlag(FileAttributes.Directory) then
            path.[1 ..]
        else
            Path.GetDirectoryName(fullPath).[rootDirectory.Length + 1 ..]
        )
    |> Seq.toList
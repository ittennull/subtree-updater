module internal SubtreeUpdater.Core.RepositoryUpdater

open System.IO
open LibGit2Sharp
open SubtreeUpdater.Core.Repository
open SubtreeUpdater.Core.Types
open SubtreeUpdater.Core.Types.Internal
    
type private SubtreePullResult =
    | Changed
    | NotChanged
    | Failure
    
type private CompletePullRequestResult =
    | Completed
    | NotCompleted
    
let private runSubtreePull subtreePull (repo: Repository) (branch: Branch) =
    let oldHeadCommitSha = lastCommitSha branch
    
    let mergedResult = subtreePull ()
    match mergedResult with
    | Ok _ ->
        let branch = repo.Head
        match oldHeadCommitSha <> lastCommitSha branch with
        | true -> Changed
        | false -> NotChanged
    | Error _ -> Failure
    
let private runCompletePr completePr pullRequest =
    match completePr with
    | Some completePr ->
        match completePr pullRequest with
        | Ok _ -> Completed
        | Error _ -> NotCompleted
    | None -> NotCompleted


let private updateSubtree (repo: Repository) defaultBranch subtreePull (existingPullRequest: PullRequest option) pushBranch createPr completePr =
    let branch =
        match existingPullRequest with
        | Some pr -> checkOutBranch repo pr.BranchName
        | None -> createBranch repo defaultBranch
        
    match runSubtreePull subtreePull repo branch with
    | Changed ->
        pushBranch repo branch
            
        let pullRequestResult = existingPullRequest |> Option.map Ok |> Option.defaultWith (fun () -> createPr branch)
        pullRequestResult
        |> Result.bind (fun pullRequest ->
            match runCompletePr completePr pullRequest with
            | Completed -> PullRequestCompleted pullRequest.Url |> Ok
            | NotCompleted -> (if existingPullRequest.IsSome then PullRequestUpdated else PullRequestCreated) pullRequest.Url |> Ok )

    | NotChanged ->
        match existingPullRequest with
        | Some pullRequest ->
            match runCompletePr completePr pullRequest with
            | Completed -> PullRequestCompleted pullRequest.Url |> Ok
            | NotCompleted -> PullRequestUnchanged pullRequest.Url |> Ok
        | None -> Ok NothingToDo

    | Failure -> Ok MergeConflict

let updateRepository remoteRepository cloneRepository findSubtreeDirectories getMapOfSubtreeDirToPullRequest subtreePull pushBranch createPr completePr =
    use repo: Repository = cloneRepository remoteRepository.Uri Normal
    let workingDirectory = repo.Info.WorkingDirectory.[.. repo.Info.WorkingDirectory.Length - 2]
    
    let updateSubtreeDirectory mapOfSubtreeDirToPullRequest localSubtreeDir =
        let subtreePull' () = subtreePull localSubtreeDir workingDirectory
        let createPr' = createPr remoteRepository localSubtreeDir
        let existingPr = Map.tryFind localSubtreeDir mapOfSubtreeDirToPullRequest
        
        let updateSubtreeResult =
            updateSubtree
                repo
                remoteRepository.DefaultBranchName
                subtreePull'
                existingPr
                pushBranch
                createPr'
                completePr
                
        let updateSubtreeResult' =
            match updateSubtreeResult with
            | Ok x -> x
            | Error error -> UpdateError error
        
        {   Repository = remoteRepository.Name
            Subtree = localSubtreeDir
            UpdateSubtreeResult = updateSubtreeResult' }
    
    let updateResults =
        match findSubtreeDirectories workingDirectory with
        | [] -> []
        | localSubtreeDirectories ->
            setUserNameAndEmailForRepository repo
            
            let updateResults =
                getMapOfSubtreeDirToPullRequest remoteRepository
                |> Result.map (fun findPullRequest ->
                    localSubtreeDirectories |> List.map (updateSubtreeDirectory findPullRequest) )
            
            match updateResults with
            | Ok results -> results
            | Error error -> [
                {   Repository = remoteRepository.Name
                    Subtree = ""
                    UpdateSubtreeResult = UpdateError error } ]

    Directory.Delete(workingDirectory, true)
    
    updateResults
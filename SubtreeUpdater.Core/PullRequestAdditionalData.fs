module internal SubtreeUpdater.Core.PullRequestAdditionalData

open System.Text.RegularExpressions

let createAdditionalDataString subtreeRepoName subtreeDirectory =
    sprintf "subtree-updater(from=%s;path=%s)" subtreeRepoName subtreeDirectory

let getMapOfSubtreeDirToPullRequest subtreeRepoName getAllPullRequests getAdditionalData createDomainPullRequest =
    let regexString = sprintf "subtree-updater\(from=%s;path=(.+)\)" subtreeRepoName
    let regex = Regex(regexString)
    
    let getSubtreeDir s =
        let m = regex.Match(s)
        match m.Success with
        | true -> Some m.Groups.[1].Value
        | false -> None
        
    try
        let map =
            getAllPullRequests()
            |> Seq.choose (fun pr -> getAdditionalData pr
                                     |> getSubtreeDir
                                     |> Option.map (fun subtreeDir -> subtreeDir, createDomainPullRequest pr))
            |> Map.ofSeq
        
        Ok map
    with
        | ex -> Error ex.Message
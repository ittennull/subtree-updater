module internal SubtreeUpdater.Core.ProcessRunner

open Microsoft.Extensions.Logging
open System.Diagnostics

let runProcess (logger: ILogger) workingDirectory filename args =
    let startInfo =
        ProcessStartInfo(
            WindowStyle = ProcessWindowStyle.Hidden,
            UseShellExecute = false,
            FileName = filename,
            Arguments = args,
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            WorkingDirectory = workingDirectory                            
        )
    
    use proc = new Process(StartInfo = startInfo)
    proc.Start() |> ignore
    proc.WaitForExit()

    logger.LogInformation(proc.StandardOutput.ReadToEnd())
    logger.LogInformation(proc.StandardError.ReadToEnd())

    if proc.ExitCode = 0 then
        Ok()
    else
        Error()

namespace SubtreeUpdater.Core.Types

open System

type PullRequestUrl = string

type UpdateSubtreeResult =
    | PullRequestCompleted of PullRequestUrl
    | PullRequestUpdated of PullRequestUrl
    | PullRequestCreated of PullRequestUrl
    | PullRequestUnchanged of PullRequestUrl
    | NothingToDo
    | MergeConflict
    | UpdateError of string

type UpdateResult = {
    Repository: string
    Subtree: string
    UpdateSubtreeResult: UpdateSubtreeResult
}

type SearchFor =
    | ExactLocation of string
    | PathEndsWith of string
    
type GitSystem = AzureDevOps | GitHub | GitLab

type RunParameters = {
    GitSystem: GitSystem option
    Pat: string
    SubtreeUrl: Uri
    SearchFor: SearchFor
    DryRun: bool
    RepositoryGroupFilters: string seq
    RepositoryNameFilters: string seq
    CompletePullRequests: bool
}

module internal Internal =
    type CustomPullRequest =
        | AzureDevOps of Microsoft.TeamFoundation.SourceControl.WebApi.GitPullRequest
        | GitHub of Octokit.PullRequest
        | GitLab of GitLabApiClient.Models.MergeRequests.Responses.MergeRequest

    type PullRequest = {
        Url: PullRequestUrl
        BranchName: string
        Custom: CustomPullRequest
    }
    
    type CustomRemoteRepository =
        | AzureDevOps of Microsoft.TeamFoundation.SourceControl.WebApi.GitRepository
        | GitHub of Octokit.Repository
        | GitLab of GitLabApiClient.Models.Projects.Responses.Project
        
    type RemoteRepository = {
        Name: string
        Uri: Uri
        DefaultBranchName: string
        GroupName: string
        Custom: CustomRemoteRepository
    }
        
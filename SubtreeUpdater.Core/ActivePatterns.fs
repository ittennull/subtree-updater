module SubtreeUpdater.Core.ActivePatterns

open SubtreeUpdater.Core.Types

let (|PullRequestCompleted|_|) result =
    match result.UpdateSubtreeResult with
    | PullRequestCompleted url -> Some (result, url)
    | _ -> None

let (|PullRequestUpdated|_|) result =
    match result.UpdateSubtreeResult with
    | PullRequestUpdated url -> Some (result, url)
    | _ -> None
    
let (|PullRequestCreated|_|) result =
    match result.UpdateSubtreeResult with
    | PullRequestCreated url -> Some (result, url)
    | _ -> None

let (|PullRequestUnchanged|_|) result =
    match result.UpdateSubtreeResult with
    | PullRequestUnchanged url -> Some (result, url)
    | _ -> None
    
let (|MergeConflict|_|) result =
    match result.UpdateSubtreeResult with
    | MergeConflict -> Some result
    | _ -> None
    
let (|NothingToDo|_|) result =
    match result.UpdateSubtreeResult with
    | NothingToDo -> Some result
    | _ -> None
    
let (|UpdateError|_|) result =
    match result.UpdateSubtreeResult with
    | UpdateError error -> Some (result, error)
    | _ -> None
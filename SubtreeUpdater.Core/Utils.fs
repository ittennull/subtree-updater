module internal SubtreeUpdater.Core.Utils

open System.Text.RegularExpressions

let createFilter filters =
    let createFilter (filter: string) =
        let filter' = "^" + filter.Replace(".", "\.").Replace("?", ".").Replace("*", ".*") + "$" 
        let regex = Regex(filter')
        regex.IsMatch
    
    let filters' = filters |> Seq.map createFilter |> Seq.toList
    
    fun x ->
        match Seq.isEmpty filters' with
        | true -> true
        | false -> filters' |> Seq.exists (fun f -> f x)
        
let getSubtreeRepositoryName (url: string) =
    let name = url.Split('/') |> Seq.last
    match name.EndsWith(".git") with
    | true -> name.[.. name.Length - 5]
    | false -> name
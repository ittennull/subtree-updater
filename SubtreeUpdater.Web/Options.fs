module SubtreeUpdater.Web.Options

open System

[<CLIMutable>]
type Preset = {
    GitSystem: string
    Name: string
    Pat: string
    SubtreeUrl: Uri
    ExactLocation: string
    PathEndsWith: string
    CompletePullRequests: bool
    RepositoryNameFilters: string seq
    DevOpsProjectNameFilters: string seq
    GitHubOrganizationNameFilters: string seq
    GitLabGroupNameFilters: string seq
    DryRun: bool
}

type Presets = ResizeArray<Preset>
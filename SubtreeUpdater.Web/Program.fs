module SampleApp.App

open System
open Microsoft.AspNetCore
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection
open MongoDB.Driver
open Hangfire
open Hangfire.Mongo
open Giraffe
open SubtreeUpdater.Web
open SubtreeUpdater.Web.Options

// ---------------------------------
// Error handler
// ---------------------------------

let errorHandler (ex : Exception) (logger : ILogger) =
    logger.LogError(EventId(), ex, "An unhandled exception has occurred while executing the request.")
    clearResponse >=> setStatusCode 500 >=> text ex.Message

// ---------------------------------
// Web app
// ---------------------------------

let webApp =
    choose [
        GET >=> route  "/" >=> warbler(fun _ -> HttpHandlers.mainView)
        POST >=> routef "/start/%s" (fun preset -> warbler(fun _ -> HttpHandlers.start preset))
        RequestErrors.notFound (text "Not Found")
        ]

// ---------------------------------
// Main
// ---------------------------------

let databaseName = "SubtreeUpdater"

let configureApp (app : IApplicationBuilder) =
    let configuration = app.ApplicationServices.GetRequiredService<IConfiguration>()
    let dashboardAuthorization: Dashboard.IDashboardAuthorizationFilter list =
        match configuration.GetValue<bool>("DisablePublicHangfireDashboard") with
        | true -> [Dashboard.LocalRequestsOnlyAuthorizationFilter()]
        | false -> []
    
    app.UseGiraffeErrorHandler(errorHandler)
       .UseStaticFiles()
       .UseHangfireDashboard("/hangfire", DashboardOptions(Authorization = dashboardAuthorization))
       .UseResponseCaching()
       .UseGiraffe webApp

let configureMongoDb (connectionString: string) (services : IServiceCollection) =
    let client = MongoClient(connectionString)
    let database = client.GetDatabase(databaseName)
    services.AddSingleton(database)

let configureServices (services : IServiceCollection) =
    services
        .AddResponseCaching()
        .AddGiraffe() |> ignore
        
    let serviceProvider = services.BuildServiceProvider()
    let connectionString = serviceProvider.GetRequiredService<IConfiguration>().GetConnectionString("MongoDb")
    configureMongoDb connectionString services |> ignore
    
    let configuration = serviceProvider.GetService<IConfiguration>()
    services.Configure<Presets>(configuration.GetSection("Presets")) |> ignore
    
    services.AddScoped<SubtreeUpdater.Web.UpdateJob.UpdateJob>() |> ignore
        
    services.AddHangfire(
        fun config ->
            let mongoClientSettings = MongoClientSettings.FromConnectionString(connectionString)
            let mongoStorageOptions = MongoStorageOptions(MigrationOptions = MongoMigrationOptions(Strategy = MongoMigrationStrategy.Drop))
            config
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseMongoStorage(mongoClientSettings, databaseName, mongoStorageOptions) |> ignore 
        )
        .AddHangfireServer() |> ignore
    ()

let configureLogging (loggerBuilder : ILoggingBuilder) =
    loggerBuilder.AddConsole()
                 .AddDebug() |> ignore

[<EntryPoint>]
let main _ =
    WebHost.CreateDefaultBuilder()
        .Configure(Action<IApplicationBuilder> configureApp)
        .ConfigureServices(configureServices)
        .ConfigureLogging(configureLogging)
        .Build()
        .Run()
    0
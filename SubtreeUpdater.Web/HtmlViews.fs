module SubtreeUpdater.Web.HtmlViews

open Giraffe.GiraffeViewEngine
open SubtreeUpdater.Web.UpdateJob
open SubtreeUpdater.Web.Options

let layout (content: XmlNode list) =
    html [] [
        head [] [
            title []  [ str "Subtree updater results" ]
            link [ _href "/site.css"; _rel "stylesheet" ]
        ]
        body [] content
    ]

let jobPartial job =
    let isFinished = job.FinishedAt.HasValue 
    let summary = summary [] [
        span [] [
            str "Preset: "
            b [] [str job.Preset]
            sprintf ", start time: %s, status: " (job.StartedAt.ToString()) |> str 
        ]
        span [ if isFinished then yield _class "success" ]
            [(match isFinished with
              | true -> sprintf "Finished at %s" (job.FinishedAt.Value.ToString()) |> str
              | false -> i [] [str "in progress"] 
             )]
    ]
    let prLine (subtreeLocation, url) =
        [
            code [] [sprintf "%s - " subtreeLocation.RepositoryName |> str]
            a [_href url] [str url]
        ]
        
    let simpleListItem subtreeLocation =
        [
            code [] [str subtreeLocation.RepositoryName]
            span [_class "dimmed"] [str " - subtree path: "]
            code [] [str subtreeLocation.SubtreePath]
        ]
        
    let errorLine (subtreeLocation, error) =
        simpleListItem subtreeLocation @ [
            sprintf ", error: %s" error |> str
        ]
        
    let listOfResults list className createListItem =
        list
        |> List.ofSeq
        |> List.map (fun x -> li [_class className] (createListItem x) )
        
    let section title results className createListItem =
        if Seq.isEmpty results then
            emptyText
        else
            p [] [
                h4 [] [str title]
                ul [] (listOfResults results className createListItem)
            ]
    
    details [] [
        yield summary
        if isFinished then
            yield section "Completed PRs" job.CompletedPullRequests "completed-pr" prLine
            yield section "Created or updated PRs" job.CreatedOrUpdatedPullRequests "created-pr" prLine
            yield section "Merge conflicts" job.MergeConflicts "merge-conflict" simpleListItem
            yield section "Nothing to do. The subtrees are up to date" job.NothingToDo "nothing-to-do" simpleListItem
            yield section "Errors" job.Errors "error" errorLine
    ]
    
let lastJobsSection jobs =
    section [] (
            [
                [ h3 [] [ str "Last jobs" ] ]
                jobs |> List.map jobPartial
            ] |> List.concat)
    
let triggerUpdaterSection presets =
    let createOption (preset: Preset) =
        option [_value preset.Name] [str preset.Name]
    
    section [_class "trigger-updater"] [
        script [] [
            rawText """
            function runPreset() {
                fetch(`/start/${document.getElementById("presets").value}`, { method: "POST" });
            }
            """
        ]
        
        div [] [
            button [_onclick "runPreset()"] [str "Run preset"]
            select [_id "presets"] (presets |> Seq.toList |> List.map createOption)
        ]
        small [_class "note"] [
            str "(makes a POST request to /start/<preset-name>)"
        ]
    ]

let mainView presets jobs =
    [
        triggerUpdaterSection presets
        hr []
        lastJobsSection jobs
    ] |> layout
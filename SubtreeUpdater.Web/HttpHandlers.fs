module SubtreeUpdater.Web.HttpHandlers

open System
open System.Net
open Giraffe
open Hangfire
open FSharp.Control.Tasks.V2.ContextInsensitive
open Microsoft.Extensions.Options
open MongoDB.Driver
open SubtreeUpdater.Web.HtmlViews
open SubtreeUpdater.Web.UpdateJob
open SubtreeUpdater.Web.Options

let start preset: HttpHandler =
    handleContext(
        fun ctx ->
            let id = Guid.NewGuid() |> ShortGuid.fromGuid
            BackgroundJob.Enqueue(fun (job: UpdateJob) -> job.Run (id, preset)) |> ignore
            
            ctx.SetStatusCode (HttpStatusCode.Accepted |> int) 
            ctx.WriteTextAsync "Job queued"
            )
    
let mainView: HttpHandler =
    handleContext(
        fun ctx -> task{
            let getJobs = task{
                let database = ctx.GetService<IMongoDatabase>()
                let jobsCollection = getJobsCollection database
                
                let sort = Builders<Job>.Sort.Descending(fun x -> x.StartedAt :> obj)
                let! cursor =
                    jobsCollection.FindAsync(
                         Builders<Job>.Filter.Empty,
                         FindOptions<Job, Job>(Sort = sort, Limit = Nullable(10)) 
                    )
                let! jobs = cursor.ToListAsync()
                return jobs |> List.ofSeq
            }
                
            let presets = ctx.GetService<IOptions<Presets>>().Value
            let! jobs = getJobs
            
            return! mainView presets jobs |> ctx.WriteHtmlViewAsync
            })
    
    
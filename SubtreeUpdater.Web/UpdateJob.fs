module SubtreeUpdater.Web.UpdateJob

open System
open Microsoft.Extensions.Logging
open Microsoft.Extensions.Options
open MongoDB.Driver
open SubtreeUpdater.Core.Types
open SubtreeUpdater.Core.ActivePatterns
open SubtreeUpdater.Web.Options

type PullRequestUrl = string
type SubtreeLocation = {
    RepositoryName: string
    SubtreePath: string
}

[<CLIMutable>]
type Job = {
    Id: string
    StartedAt: DateTime
    FinishedAt: Nullable<DateTime>
    Preset: string
    CompletedPullRequests: (SubtreeLocation*PullRequestUrl) seq
    CreatedOrUpdatedPullRequests: (SubtreeLocation*PullRequestUrl) seq
    MergeConflicts: SubtreeLocation seq
    NothingToDo: SubtreeLocation seq
    Errors: (SubtreeLocation*string) seq
}

let getJobsCollection (db: IMongoDatabase) = db.GetCollection<Job>("jobs")

let private toOption = function | null | "" -> None | x -> Some x
let private emptySeqIfNull = function | null -> Seq.empty | x -> x

let private getSearchFor exactLocation pathEndsWith =
    match exactLocation, pathEndsWith with
    | Some path, None -> ExactLocation path
    | None, Some pathEndsWith -> PathEndsWith pathEndsWith
    | _ -> failwith "Either 'ExactLocation' or 'PathEndsWith' must be specified"
    
let private updateJob job updateResults =
    let selector patterns x =
        patterns
        |> Seq.map ((|>) x)
        |> Seq.choose id
        |> Seq.tryHead
            
    let choose patterns map =
        updateResults
        |> List.choose (selector patterns)
        |> List.map map
        
    let createSubtreeLocation result = {RepositoryName = result.Repository; SubtreePath = result.Subtree}
    let createSubtreeLocationWithUrl (result, url) = createSubtreeLocation result, url
    let createSubtreeLocationWithError (result, error) = createSubtreeLocation result, error
    
    {job with
        CompletedPullRequests = choose [(|PullRequestCompleted|_|)] createSubtreeLocationWithUrl
        CreatedOrUpdatedPullRequests = choose [(|PullRequestCreated|_|); (|PullRequestUpdated|_|); (|PullRequestUnchanged|_|)] createSubtreeLocationWithUrl
        MergeConflicts = choose [(|MergeConflict|_|)] createSubtreeLocation
        NothingToDo = choose [(|NothingToDo|_|)] createSubtreeLocation
        Errors = choose [(|UpdateError|_|)] createSubtreeLocationWithError }

let private runJob jobId presetName (db: IMongoDatabase) (presets: Preset seq) (logger: ILogger) =
    let preset = presets |> Seq.find (fun x -> x.Name = presetName)
    
    let gitSystem =
        toOption preset.GitSystem
        |> Option.map(function
            | "azuredevops" -> AzureDevOps
            | "github" -> GitHub
            | "gitlab" -> GitLab
            | _ -> failwith "Unsupported value of 'GitSystem' parameter")
        
    let repositoryGroupFilters =
        let notEmptyFilters =
            [ preset.DevOpsProjectNameFilters
              preset.GitHubOrganizationNameFilters
              preset.GitLabGroupNameFilters ]
            |> List.map emptySeqIfNull
            |> List.filter (Seq.isEmpty >> not)
        
        match notEmptyFilters.Length with
        | n when n <= 1 -> notEmptyFilters |> List.tryHead |> Option.defaultValue Seq.empty 
        | _ -> failwith "Either single one of 'DevOpsProjectNameFilters' or 'GitHubOrganizationNameFilters' or 'GitLabGroupNameFilters' may be specified"
        
    let jobs = getJobsCollection db
    let filter = Builders<Job>.Filter.Eq((fun x -> x.Id), jobId)
    
    let job = {
        Id = jobId
        StartedAt = DateTime.UtcNow
        FinishedAt = Nullable<DateTime>()
        Preset = presetName
        CompletedPullRequests = []
        CreatedOrUpdatedPullRequests = []
        MergeConflicts = []
        NothingToDo = []
        Errors = []
    }
    jobs.ReplaceOne(filter, job, ReplaceOptions(IsUpsert = true)) |> ignore
    
    let runParameters = {
        GitSystem = gitSystem
        Pat = preset.Pat
        SubtreeUrl = preset.SubtreeUrl
        SearchFor = getSearchFor (toOption preset.ExactLocation) (toOption preset.PathEndsWith)
        RepositoryGroupFilters = repositoryGroupFilters
        RepositoryNameFilters = emptySeqIfNull preset.RepositoryNameFilters
        CompletePullRequests = preset.CompletePullRequests
        DryRun = preset.DryRun
    }
    
    let updateResults = SubtreeUpdater.Core.Runner.run runParameters logger
    
    let job = updateJob job updateResults
    let job = {job with FinishedAt = Nullable<DateTime>(DateTime.UtcNow)}
    jobs.ReplaceOne(filter, job) |> ignore
    
    logger.LogInformation("Done")
    
    
type UpdateJob(database: IMongoDatabase, presets: IOptions<Presets>, logger: ILogger<UpdateJob>) =
    member this.Run (jobId, presetName) = runJob jobId presetName database presets.Value logger